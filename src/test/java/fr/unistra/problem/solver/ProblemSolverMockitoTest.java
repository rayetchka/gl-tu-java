package fr.unistra.problem.solver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;

import fr.unistra.fizzbuzz.FizzBuzz;

public class ProblemSolverMockitoTest {

    private Int2String mockInt2String;
    private Displayer mockDisplayer;
    private ProblemSolver solver;

    @Before public void initSolver() {
        mockInt2String = Mockito.mock(Int2String.class);
        mockDisplayer = Mockito.mock(Displayer.class);
        solver = new ProblemSolver(mockInt2String, mockDisplayer);
    }

    @Test
    public void testZeroNAppelleNiConvertNiDisplay() {
        solver.solve(0);
        Mockito.verify(mockInt2String, never()).convert(anyInt());
        Mockito.verify(mockDisplayer, never()).display(anyString());
    }

    @Test
    public void testUnAppelleConvertDeUnEtTransmetLaReponseADisplay() {
        Mockito.when(mockInt2String.convert(anyInt()))
                .thenReturn("valeur test de retour");

        solver.solve(1);
        ArgumentCaptor<Integer> capture1 = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(mockInt2String).convert(capture1.capture());
        ArgumentCaptor<String> capture2 = ArgumentCaptor.forClass(String.class);
        Mockito.verify(mockDisplayer).display(capture2.capture());

        assertEquals(Integer.valueOf(1), capture1.getValue());
        assertEquals("valeur test de retour", capture2.getValue());
    }

    @Test
    public void testCinqAppelleCinqFoisConvertEtDipslay() {
        solver.solve(5);
        Mockito.verify(mockInt2String, times(5)).convert(anyInt());
        Mockito.verify(mockDisplayer, times(5)).display(anyString());
    }

    @Test
    public void testCinqAppelleCinqFoisConvertDeUnACinq() {
        solver.solve(5);
        ArgumentCaptor<Integer> capture1 = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(mockInt2String, times(5)).convert(capture1.capture());
        assertThat(capture1.getAllValues())
            .containsExactly(
                    Integer.valueOf(1),
                    Integer.valueOf(2),
                    Integer.valueOf(3),
                    Integer.valueOf(4),
                    Integer.valueOf(5)
            );
    }

    @Test
    public void testSpy() {
        FizzBuzz fizzBuzz = Mockito.spy(new FizzBuzz());
        solver = new ProblemSolver(fizzBuzz, mockDisplayer);

        solver.solve(3 * 5);

        ArgumentCaptor<Integer> capture1 = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(fizzBuzz, atLeastOnce()).convert(capture1.capture());

        ArgumentCaptor<String> capture2 = ArgumentCaptor.forClass(String.class);
        Mockito.verify(mockDisplayer, atLeastOnce()).display(capture2.capture());

        assertThat(capture1.getAllValues())
            .contains(3 * 5);

        assertThat(capture2.getAllValues())
            .contains("FizzBuzz");

    }

    @Test
    public void testException() {
        Mockito.when(mockInt2String.convert(eq(7)))
            .thenThrow(new NumberFormatException());

        try {
            solver.solve(5);
        } catch (NumberFormatException ex) {
            throw ex;
        }

        try {
            solver.solve(10);
            fail();
        } catch (NumberFormatException ex) {
            /* succès */
        }
    }

}
