package fr.unistra.problem.solver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class ProblemSolverTest {

    private final class StubDisplayer implements Displayer {
        private List<String> capture = new LinkedList<>();

        @Override
        public void display(String valeur) {
            capture.add(valeur);
        }
    }

    private final class StubInt2String implements Int2String {
        @Override
        public String convert(int i) {
            return "valeur " + i;
        }
    }

    @Test
    public void testStubsZero() {
        StubInt2String stubInt2String = new StubInt2String();
        StubDisplayer capture = new StubDisplayer();
        ProblemSolver solver = new ProblemSolver(stubInt2String, capture);

        solver.solve(0);
        assertTrue(capture.capture.isEmpty());
    }

    @Test
    public void testStubs1() {
        StubInt2String stubInt2String = new StubInt2String();
        StubDisplayer capture = new StubDisplayer();
        ProblemSolver solver = new ProblemSolver(stubInt2String, capture);

        solver.solve(1);
        assertFalse(capture.capture.isEmpty());
        assertEquals("valeur 1", capture.capture.get(0));
    }

    @Test
    public void testStubs5() {
        StubInt2String stubInt2String = new StubInt2String();
        StubDisplayer capture = new StubDisplayer();
        ProblemSolver solver = new ProblemSolver(stubInt2String, capture);

        solver.solve(5);
        assertThat(capture.capture)
            .containsExactly(
                    "valeur 1",
                    "valeur 2",
                    "valeur 3",
                    "valeur 4",
                    "valeur 5"
            );
    }

}
