package fr.unistra.fizzbuzz;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class FizzBuzzJUnitTest {

	private FizzBuzz fizzbuzz = new FizzBuzz();

	@Test
	public void returnsNumberForInputNotDivisibleByThreeAndFive() {
		assertEquals("1", fizzbuzz.convert(1));
		assertEquals("2", fizzbuzz.convert(2));
		assertEquals("4", fizzbuzz.convert(4));
		assertEquals("7", fizzbuzz.convert(7));
		assertEquals("11",fizzbuzz.convert(11));
		assertEquals("13",fizzbuzz.convert(13));
		assertEquals("14",fizzbuzz.convert(14));
	}

	@Test
	public void divisiblePar3() {
	    assertEquals("Fizz", fizzbuzz.convert(2*3));
	}

	@Test
	public void divisiblePar5() {
	    assertEquals("Buzz", fizzbuzz.convert(5*5));
	}

	@Test
	public void divisiblePar3Et5() {
	    assertEquals("FizzBuzz", fizzbuzz.convert(2*3*5));
	}
	
	@Test
	public void divisibleParNi3Ni5() {
	    assertEquals("31", fizzbuzz.convert(31));
	}

	@Test
	public void pour1() {
	    assertEquals("1", fizzbuzz.convert(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void pasInferieurA1() {
	    fizzbuzz.convert(0);
	    fail();
	}

}
