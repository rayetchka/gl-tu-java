package fr.unistra.problem.solver;

public interface Int2String {

    String convert(int i);

}
