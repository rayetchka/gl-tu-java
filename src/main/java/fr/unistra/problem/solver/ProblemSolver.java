package fr.unistra.problem.solver;

public class ProblemSolver {

    private final Int2String int2String;
    private final Displayer displayer;

    public ProblemSolver(
            Int2String int2String,
            Displayer displayer
    ) {
        this.int2String = int2String;
        this.displayer = displayer;
    }

    public void solve(int max) {
        for (int i = 1; i <= max; i++)
            displayer.display(int2String.convert(i));
    }

}
