package fr.unistra.fizzbuzz;

import fr.unistra.problem.solver.Int2String;

public class FizzBuzz implements Int2String {

    public String convert(int number) {
        if (number < 1)
            throw new IllegalArgumentException("doit être positif strict");
        boolean isDiv3 = number % 3 == 0;
        boolean isDiv5 = number % 5 == 0;
        if (isDiv3 && isDiv5)
            return "FizzBuzz";
        else if (isDiv3)
            return "Fizz";
        else if (isDiv5)
            return "Buzz";
        else
            return Integer.toString(number);
    }

}
