package fr.unistra.fizzbuzz;

import java.util.Scanner;

import fr.unistra.problem.solver.ProblemSolver;

public class Main {

    public static void main(String[] args) {
        ProblemSolver solver = new ProblemSolver(new FizzBuzz(), new ConsoleDisplay());
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nombre=");
        solver.solve(scanner.nextInt());
    }

}
