package fr.unistra.fizzbuzz;

import fr.unistra.problem.solver.Displayer;

public class ConsoleDisplay implements Displayer {

    @Override
    public void display(String message) {
        System.out.println(message);
    }

}
